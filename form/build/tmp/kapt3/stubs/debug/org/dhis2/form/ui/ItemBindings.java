package org.dhis2.form.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0007\u00a8\u0006\t"}, d2 = {"Lorg/dhis2/form/ui/ItemBindings;", "", "()V", "setImeOption", "", "editText", "Landroid/widget/EditText;", "type", "Lorg/dhis2/form/model/KeyboardActionType;", "form_debug"})
public final class ItemBindings {
    public static final org.dhis2.form.ui.ItemBindings INSTANCE = null;
    
    @androidx.databinding.BindingAdapter(value = {"setImeOption"})
    public static final void setImeOption(@org.jetbrains.annotations.NotNull()
    android.widget.EditText editText, @org.jetbrains.annotations.Nullable()
    org.dhis2.form.model.KeyboardActionType type) {
    }
    
    private ItemBindings() {
        super();
    }
}