package dhis2.org.analytics.charts.mappers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001e\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\u001a\n\u0010\u0002\u001a\u00020\u0003*\u00020\u0003\u001a\n\u0010\u0002\u001a\u00020\u0004*\u00020\u0004\u001a\n\u0010\u0002\u001a\u00020\u0005*\u00020\u0005\u001a\u0012\u0010\u0006\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\u0007\u001a\u00020\b\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"default_value_text_size", "", "withGlobalStyle", "Lcom/github/mikephil/charting/components/Legend;", "Lcom/github/mikephil/charting/data/BarData;", "Lcom/github/mikephil/charting/data/LineDataSet;", "withNutritionBackgroundGlobalStyle", "dataSetColor", "", "dhis_android_analytics_dhisDebug"})
public final class ChartsGlobalStylesKt {
    public static final float default_value_text_size = 10.0F;
    
    @org.jetbrains.annotations.NotNull()
    public static final com.github.mikephil.charting.data.LineDataSet withGlobalStyle(@org.jetbrains.annotations.NotNull()
    com.github.mikephil.charting.data.LineDataSet $this$withGlobalStyle) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.github.mikephil.charting.data.BarData withGlobalStyle(@org.jetbrains.annotations.NotNull()
    com.github.mikephil.charting.data.BarData $this$withGlobalStyle) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.github.mikephil.charting.components.Legend withGlobalStyle(@org.jetbrains.annotations.NotNull()
    com.github.mikephil.charting.components.Legend $this$withGlobalStyle) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.github.mikephil.charting.data.LineDataSet withNutritionBackgroundGlobalStyle(@org.jetbrains.annotations.NotNull()
    com.github.mikephil.charting.data.LineDataSet $this$withNutritionBackgroundGlobalStyle, int dataSetColor) {
        return null;
    }
}